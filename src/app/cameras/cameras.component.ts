import { Component, OnInit, ViewChild } from '@angular/core';
import { CameraModel, RecordModel } from '../_models';
import { MapComponent } from './map/map.component';
import { AuthenticationService, CameraService } from '../_services';
import { MatDialog } from '@angular/material/dialog';
import { DialogContentFilter } from '../_shared/filter-selector.component';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { MatTabChangeEvent } from '@angular/material/tabs';

@Component({
  selector: 'app-cameras',
  templateUrl: './cameras.component.html',
  styleUrls: ['./cameras.component.scss'],
})
export class CamerasComponent implements OnInit {
  userRole: string;
  tabName = 'map';
  recordSideNavOpened = true;
  camerasCompany: CameraModel[] = [];
  selectedCamera: CameraModel = {};
  selectedRecord: RecordModel = {};
  filteredCameras: CameraModel[] = [];
  selectedSituation: number = -1;
  selectedSeverity: number = -1;
  isLoaded: boolean = false;

  @ViewChild(MapComponent)
  private mapComponent: MapComponent;

  constructor(
    public dialog: MatDialog,
    private cameraService: CameraService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private authService: AuthenticationService
  ) {
    this.matIconRegistry.addSvgIcon(
      'camera',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        '../../assets/images/ipcamera.svg'
      )
    );
    this.matIconRegistry.addSvgIcon(
      'redCircle',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        '../../assets/images/icons/red_circle.svg'
      )
    );
    this.matIconRegistry.addSvgIcon(
      'yellowCircle',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        '../../assets/images/icons/yellow_circle.svg'
      )
    );
    this.matIconRegistry.addSvgIcon(
      'greenCircle',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        '../../assets/images/icons/green_circle.svg'
      )
    );
  }

  ngOnInit(): void {
    this.userRole = this.authService.currentUser.role;
    this.reloadServices();
  }

  reloadServices(selectCameraId?) {
    this.camerasCompany = [];
    this.cameraService.getAllCameras().subscribe(
      (res) => {
        res['rows'].forEach((camera: CameraModel, idx: number) => {
          camera.status = camera.online ? 'online' : 'offline';
          camera.records = camera.records.reverse();
          camera.severityMean =
            camera.records
              .map((r) => r.severityLevel)
              .reduce((a, b) => a + b, 0) / camera.records.length;
          camera.severityMeanStr = camera.severityMean.toFixed(2);
          if (selectCameraId) {
            if (selectCameraId === camera.id) {
              this.selectInitialCamera(camera);
            }
          } else if (idx === 0) {
            this.selectInitialCamera(camera);
          } else {
            camera.selected = false;
          }
          this.camerasCompany.push(camera);
        });
      },
      (err) => {
        console.warn('cameras loading error', err);
      },
      () => {
        this.filterCameras();
        this.isLoaded = true;
      }
    );
  }

  private selectInitialCamera(camera: CameraModel) {
    camera.selected = true;
    this.selectedCamera = camera;
    if (camera.records.length) {
      this.selectRecord(camera.records[0].id);
    }
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogContentFilter, {
      data: {
        selectedSituation: this.selectedSituation,
        selectedSeverity: this.selectedSeverity,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.selectedSituation = result.selectedSituation;
        this.selectedSeverity = result.selectedSeverity;
        this.filterCameras();
      }
    });
  }

  filterCameras() {
    this.filteredCameras = [];
    this.camerasCompany.forEach((cam) => {
      // filter by situation
      let situation = this.selectedSituation == 1 ? false : true;
      if (
        (this.selectedSeverity == Math.round(cam.severityMean) ||
          this.selectedSeverity == -1) &&
        (situation == cam.active || this.selectedSituation == -1)
      ) {
        this.filteredCameras.push(cam);
        cam.showOnMap = true;
      } else {
        cam.showOnMap = false;
      }
    });
    if (this.filteredCameras.length) {
      this.selectCamera(this.filteredCameras[0].id);
    }
    return this.filteredCameras;
  }

  //TODO ver se essa função é redundante com a de cima
  selectCamera(cameraId: string) {
    this.camerasCompany.forEach((cam) => {
      if (cam.id == cameraId) {
        cam.selected = true;
        this.selectedCamera = cam;
        if (cam.records.length) {
          this.selectRecord(cam.records[0].id);
        }
      } else {
        cam.selected = false;
      }
    });
  }

  selectRecord(recordId: string) {
    if (!recordId) {
      if (this.selectedCamera.records.length) {
        this.selectRecord(this.selectedCamera.records[0]);
      } else {
        this.selectedRecord = undefined;
      }
    } else {
      this.selectedCamera.records.forEach((r) => {
        if (r.id == recordId) {
          r.selected = true;
          this.selectedRecord = r;
        } else {
          r.selected = false;
        }
      });
    }
  }

  getCameraStatusTooltip(c: CameraModel) {
    return c.active == false && c.calibrated == false
      ? 'Câmera Inativa e Descalibrada'
      : '' || (c.active == false && c.calibrated)
      ? 'Câmera Inativa e Calibrada'
      : '' || (c.active && c.calibrated == false)
      ? 'Câmera Ativa e Descalibrada'
      : '' || (c.active && c.calibrated)
      ? 'Câmera Ativa e Calibrada'
      : '';
  }

  getCameraClass(c: CameraModel) {
    let style = '';
    if (!c.active) style += 'disableCamera ';
    if (c.selected) style += 'camera-selected ';
    return style;
  }

  getDangerousLevelString(d: number): string {
    let str = '';
    switch (d) {
      case 0:
        str = 'Sem risco de contaminação';
        break;
      case 1:
        str = 'Baixo risco de contaminação';
        break;
      case 2:
        str = 'Médio risco de contaminação';
        break;
      case 3:
        str = 'Alto risco de contaminação';
        break;
      default:
        str = 'Sem informações de contaminação';
        break;
    }
    return str;
    // return '<label class="label_popup">Nível: ' + str + "</label></br/>";
  }

  onTabChange(event: MatTabChangeEvent) {
    this.tabName = event.tab.textLabel;
    if (this.tabName != 'map')
      this.recordSideNavOpened = false
    else
      this.recordSideNavOpened = true
  }
  
  onCameraChanged(cameraId: string) {
    this.selectCamera(cameraId);
  }

  onRecordChanged(recordId: string) {
    this.selectRecord(recordId);
  }

  private updateCamera(updatedCamera: CameraModel) {
    this.camerasCompany.forEach((camera: any) => {
      if (camera.id == updatedCamera.id) {
        updatedCamera.status = updatedCamera.online ? 'online' : 'offline';
        updatedCamera.severityMean =
          updatedCamera.records
            .map((r) => r.severityLevel)
            .reduce((a, b) => a + b, 0) / camera.records.length;
        this.camerasCompany.splice(
          this.camerasCompany.indexOf(camera),
          1,
          updatedCamera
        );
        this.selectCamera(updatedCamera.id);
      }
    });
  }
}
