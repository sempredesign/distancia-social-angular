import { Component, AfterViewInit, Input, Output, EventEmitter, SimpleChanges} from '@angular/core';
import { CameraModel, RecordModel } from '../../_models';
import { AuthenticationService } from '../../_services';
import { environment } from '../../../environments/environment';
import * as L from 'leaflet';
import 'leaflet.markercluster';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements AfterViewInit {
  map: L.Map;
  popupOpened: boolean;
  initialMapCoordinates: L.LatLng[];
  cameraLayers = L.markerClusterGroup();
  popUp: L.Popup;
  allowResetZoom = false;
  currentRecord: RecordModel;
  currentRecordIndex = 0;
  markers: any;
  @Input() cameras: CameraModel[];
  @Input() currentCamera: CameraModel;
  @Output() newSelectedCameraId = new EventEmitter<string>();
  @Output() newSelectedRecordId = new EventEmitter<string>();

  constructor(private authService: AuthenticationService) {
    this.cameraLayers.on('clustermouseover', (a) => {
      a.layer.spiderfy();
    });
    this.cameraLayers.on('clustermouseout', (a) => {
      a.layer.unspiderfy();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.cameras){
      this.cameras = changes.cameras.currentValue;
    }
    if (this.map) {
      this.createCameraMarkers();
      this.map.addLayer(this.cameraLayers);
      if (this.allowResetZoom) {
        this.setupMapZoom();
        this.allowResetZoom = false;
      }
    }
  }

  ngAfterViewInit() {
    this.map = L.map("map", {
      preferCanvas: true,
      zoomControl: false,
      maxZoom: 19,
      keyboard: false,
      layers: [],
    });
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(this.map);
    
    this.map.on('click', () => {
      this.closeCameraPopup();
    })
    this.popupOpened = false;
    setTimeout(() => {
      this.setupMapZoom();
      this.createCameraMarkers();
      if (!this.cameras.length)
        this.allowResetZoom = true;
    }, 500);
    this.map.addLayer(this.cameraLayers);

  }

  createCameraMarkers() {
    this.cameraLayers.clearLayers();
    this.markers = {};
    this.cameras.forEach((camera) => {
      if (camera.lat && camera.lng && camera.showOnMap) {
        let severidade = 'safe';
        if (camera.severityMean > 2.5) {
          severidade = 'danger';
        } else if (camera.severityMean > 1.0) {
          severidade = 'medium';
        }
        if (!camera.active) severidade = 'offline';
        if (!camera.online) severidade = 'deffect';

        let icon = this.setIconProperties(45, 50, 100, 40, severidade, camera.name);
        let coordinate = L.latLng(camera.lat, camera.lng);
        let marker = this.setMarkerProperties(camera, icon, coordinate);

        // add a popup when clicked
        marker.on('click', (event: any) => {
          this.cameraClickContent(camera);
        });

        this.markers[camera.name] = marker;
        this.cameraLayers.addLayer(marker);
      }
    });
  }

  removeCameraLayers() {
    this.cameraLayers.clearLayers();
  }

  async delay(ms: number) {
    await new Promise((resolve) => setTimeout(() => resolve(), ms)).then(() => {
      this.removeCameraLayers();
      this.createCameraMarkers();
    });
  }

  refreshCameraMarkers() {
    this.delay(100);
  }

  private setIconProperties( icaX: number, icaY: number, icsX: number, icsY: number, className: string, identifier: string
  ): L.DivIcon {
    return L.divIcon({
      iconAnchor: new L.Point(icaX, icaY),
      iconSize: new L.Point(icsX, icsY),
      className: 'my-div-icon ' + className,
      html: ' ' + identifier,
    });
  }

  private setMarkerProperties( camera: any, icon: L.DivIcon, coordinate: L.LatLng): L.Marker {
    let marker = L.marker(coordinate, { icon: icon });
    marker['name'] = camera.name;
    marker['cameraId'] = camera.id;
    return marker;
  }

  private setupMapZoom() {
    this.initialMapCoordinates = this.cameras.map((cam) => {
      return L.latLng(cam.lat, cam.lng);
    });
    if (this.initialMapCoordinates && this.initialMapCoordinates.length > 0) {
      this.map.fitBounds(L.polyline(this.initialMapCoordinates).getBounds());
      this.map.setZoom(this.map.getZoom() - 1);
      this.map.invalidateSize();
    } else {
      this.map.fitBounds([
        [-20.2894591, -40.2926235],
        [-20.2897025, -40.2920615],
      ]);
      this.map.setZoom(16);
      this.map.invalidateSize();
    }
  }

  emitCameraRecords(camera: CameraModel, recordIdx: number) {
    this.newSelectedCameraId.emit(camera.id);
    if (camera.records.length) {
      recordIdx = recordIdx >= camera.records.length ? 0 : recordIdx;
      this.newSelectedRecordId.emit(camera.records[recordIdx].id);
    } else {
      this.newSelectedRecordId.emit(null);
    }
  }

  setModalImage(camera: CameraModel, recordIdx: number) {
    // setup modal image
    if (camera.active && camera.records && camera.records.length) {
      let img = document.getElementById(`imgSmall-${camera.id}-${recordIdx}`);
      let modal = document.getElementById(`myModal-${camera.id}-${recordIdx}`);
      let modalImg = document.getElementById(
        `imgLarge-${camera.id}-${recordIdx}`
      );
      img.addEventListener('click', () => {
        modal.style.display = 'block';
        modalImg.setAttribute('src', img.getAttribute('src'));
      });
      modal.addEventListener('click', () => {
        modal.style.display = 'none';
      });
    }
  }

  cameraSpiderEvent(camera: CameraModel) {
    let parent = this.cameraLayers.getVisibleParent(this.markers[camera.name]);
    if (typeof parent['spiderfy'] === 'function') {
      parent['spiderfy']();
    }
  }

  cameraClickContent(camera: CameraModel) {
    if (this.popupOpened && this.currentCamera.id === camera.id) {
      this.closeCameraPopup();
    } else {
      if (this.currentCamera.id != camera.id) {
        this.closeCameraPopup();
      }
      this.currentRecord = camera.records ? camera.records[0] : null;
      this.currentCamera = camera;
      let coordinate = L.latLng(camera.lat, camera.lng);
      let contentString = this.popupContentString(camera, camera.active, 0);
      this.popUp = L.popup({
        offset: [0, -50],
        closeButton: false,
      })
        .setLatLng(coordinate)
        .setContent(contentString)
        .openOn(this.map);
      this.popupOpened = true;
    }
    this.setModalImage(camera, 0);
    this.emitCameraRecords(camera, 0);
  }

  recordClickContent(camera: CameraModel, recordIdx: number) {
    this.currentRecord = camera.records[recordIdx];
    let coordinate = L.latLng(camera.lat, camera.lng);
    let contentString = this.popupContentString(
      camera,
      camera.active,
      recordIdx
    );
    this.popUp = L.popup({
      offset: [0, -50],
      closeButton: false,
    })
      .setLatLng(coordinate)
      .setContent(contentString)
      .openOn(this.map);
    this.setModalImage(camera, recordIdx);
    this.popupOpened = true;
    // this.emitCameraRecords(camera, 0);
  }

  getDistancingRiskLevel(event: any): number {
    if (!event.peopleDetected) {
      return 0;
    }
    let ratio = event.crowd / event.peopleDetected;
    if (ratio >= 0.75) {
      return 3;
    }
    if (ratio >= 0.5) {
      return 2;
    }
    if (ratio >= 0.05) {
      return 1;
    }
    return 0;
  }

  getFaceMaskRiskLevel(event: any): number {
    if (!event.facesDetected) {
      return 0;
    }
    let ratio = event.noMask / event.facesDetected;
    if (ratio >= 0.75) {
      return 3;
    }
    if (ratio >= 0.5) {
      return 2;
    }
    if (ratio > 0) {
      return 1;
    }
    return 0;
  }

  private closeCameraPopup() {
    if (this.popUp) this.popUp.closePopup();
    this.popupOpened = false;
  }

  private popupContentString(
    camera: CameraModel,
    cameraActive: boolean,
    recordIdx: number
  ): string {
    let options = {
      // dateStyle: "medium",
      year: 'numeric',
      month: 'short',
      day: '2-digit',
      // timeStyle: "medium",
    };

    if (!camera.records || camera.records.length === 0 || !this.currentRecord) {
      return `          
        <div id="rootDiv" style="width: 200px">
          <p style="text-align: center;" >Data: ${new Date(
            camera.updatedAt
          ).toLocaleDateString('pt-BR', options)}</p>
          <p style="text-align: center;" > Não há registro selecionado</p>
        </div>
      `;
    }

    let imageUrl = environment.baseUrl + '/' + this.currentRecord.imgPath;

    let severityLevel = 'Leve';
    let color = 'green';
    if (this.currentRecord.severityLevel == 1) {
      severityLevel = 'Médio';
      color = 'lightbrown';
    } else if (this.currentRecord.severityLevel == 2) {
      severityLevel = 'Grave';
      color = 'orange';
    } else if (this.currentRecord.severityLevel == 3) {
      severityLevel = 'Gravíssimo';
      color = 'red';
    }

    let peopleDetected = this.currentRecord.peopleDetected;
    let facesDetected = this.currentRecord.facesDetected;
    let crowdNumber = this.currentRecord.crowdNumber;
    let noMaskNumber = this.currentRecord.noMaskNumber;
    if (cameraActive) {
      return `
        <div id="rootDiv" style="width: 200px">
          <style>
            #myImg {
              border-radius: 5px;
              cursor: pointer;
              transition: 0.3s;
            }

            .modal {
              display: none; /* Hidden by default */
              position: fixed; /* Stay in place */
              z-index: 1; /* Sit on top */
              padding-top: 70px; /* Location of the box */
              width: 100%; /* Full width */
              height: 100%; /* Full height */
              overflow: auto; /* Enable scroll if needed */
              background-color: rgb(0,0,0); /* Fallback color */
              background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
              position: fixed;
              top: 50%;
              left: 50%;
              transform: translate(-50%, -25%);
            }

            .modal-content {
              margin: auto;
              display: block;
              width: 80%;
              max-width: 700px;
            }

            #caption-${camera.id} {
              margin: auto;
              display: block;
              width: 80%;
              max-width: 700px;
              text-align: center;
              color: #ccc;
              padding: 10px 0;
              height: 30px;
            }

            .modal-content, #caption-${camera.id} {
              animation-name: zoom;
              animation-duration: 0.6s;
            }

            @keyframes zoom {
              from {transform:scale(0)}
              to {transform:scale(1)}
            }

            .close {
              position: absolute;
              top: 15px;
              right: 35px;
              color: #f1f1f1;
              font-size: 40px;
              font-weight: bold;
              transition: 0.3s;
            }

            @media only screen and (max-width: 700px){
              .modal-content {
                width: 100%;
              }
            }
          </style>
          <p>Data: ${new Date(camera.updatedAt).toLocaleDateString(
            'pt-BR',
            options
          )}</p>
          <p style="color: ${color};">Nível: ${severityLevel}</p>
          <p>Pessoas: ${peopleDetected} </p>
          <p>Faces: ${facesDetected} </p>
          <p>Aglomeração: ${crowdNumber} </p>
          <p>Sem Máscara: ${noMaskNumber} </p>
          <div id="video-canvas">
            <img width="100%" id="imgSmall-${
              camera.id
            }-${recordIdx}" align="center" src="${imageUrl}">
          </div>
          <div id="myModal-${
            camera.id
          }-${recordIdx}" style="width: 50vw; height: 40vw;" class="modal">
            <img class="modal-content" id="imgLarge-${camera.id}-${recordIdx}">
            <div id="caption"></div>
          </div>
        </div>
      `;
    }

    return `          
      <div id="rootDiv" style="width: 200px">
        <p style="text-align: center;" >Data: ${new Date(
          camera.updatedAt
        ).toLocaleDateString('pt-BR', options)}</p>
        <p style="text-align: center;" > Câmera Inativa</p>
      </div>
    `;
  }
}
