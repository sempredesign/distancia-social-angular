import {
  Component,
  OnInit,
  AfterViewInit,
  OnChanges,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { AuthenticationService, RecordService } from '../../_services';
import { CameraModel, RecordModel } from '../../_models';
import { CameraService } from '../../_services';
import flvjs from 'flv.js';

@Component({
  selector: 'app-calibration',
  templateUrl: './calibration.component.html',
  styleUrls: ['./calibration.component.scss'],
})
export class CalibrationComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() currentCamera: CameraModel;
  @Output() calibrationDone = new EventEmitter<string>();
  user: any;
  cameraRecords: RecordModel[] = [];
  x: number;
  y: number;
  listCorners = [];
  currentLink = undefined;
  allowedToCalibrate = false;
  flvPlayer = undefined;
  isLoaded = false;

  constructor(
    private authService: AuthenticationService,
    private cameraService: CameraService
  ) {
    this.user = this.authService.currentUser;
  }

  mouseEvent(e: MouseEvent) {
    this.x = e.offsetX;
    this.y = e.offsetY;
  }

  resetRect() {
    this.listCorners = [];
    this.x = undefined;
    this.y = undefined;
    let btnFinCalib = document.getElementById('btn-final-calib');
    btnFinCalib.hidden = true;
    let c = <HTMLCanvasElement>document.getElementById('myCanvas');
    let ctx = c.getContext('2d');
    ctx.clearRect(0, 0, this.getNormalizedWidth(), this.getNormalizedHeight());
  }

  checkClickCondition(event: MouseEvent) {
    if (this.allowedToCalibrate && event.offsetY < this.getNormalizedHeight()) {
      this.x = event.offsetX;
      this.y = event.offsetY;
      if (this.listCorners.length < 4) {
        this.listCorners.push([this.x, this.y]);
      }
      this.drawRect(this.listCorners);
      if (this.listCorners.length == 4) {
        let btnFinCalib = document.getElementById('btn-final-calib');
        btnFinCalib.hidden = false;
      }
    }
  }

  drawPrevRect() {
    if (!this.allowedToCalibrate) {
      if (this.currentCamera.calibrationPoints != undefined) {
        this.currentCamera.calibrationPoints.forEach((coord, idx) => {
          if (
            idx % 2 == 0 &&
            idx < this.currentCamera.calibrationPoints.length - 1
          ) {
            let currentOriginalX = this.currentCamera.calibrationPoints[idx];
            let currentOriginalY = this.currentCamera.calibrationPoints[
              idx + 1
            ];
            let percentagePointX =
              currentOriginalX / Number(this.currentCamera.secondaryWidth);
            let percentagePointY =
              currentOriginalY / Number(this.currentCamera.secondaryHeight);
            let currentNormalizedX =
              percentagePointX * this.getNormalizedWidth();
            let currentNormalizedY =
              percentagePointY * this.getNormalizedHeight();
            this.listCorners.push([currentNormalizedX, currentNormalizedY]);
          }
        });
        if (this.listCorners.length == 4) {
          this.drawRect(this.listCorners);
        }
      }
    }
  }

  drawRect(listCornersPoly) {
    let c = <HTMLCanvasElement>document.getElementById('myCanvas');
    c.width = this.getNormalizedWidth();
    c.height = this.getNormalizedHeight();
    //c.style.transform = "translateY(-" + c.height +"px)";
    let ctx = c.getContext('2d');
    ctx.strokeStyle = '#FFFF00';
    ctx.beginPath();
    if (listCornersPoly.length >= 2) {
      listCornersPoly.forEach((coord, idx) => {
        if (idx < listCornersPoly.length - 1) {
          ctx.moveTo(listCornersPoly[idx][0], listCornersPoly[idx][1]);
          ctx.lineTo(listCornersPoly[idx + 1][0], listCornersPoly[idx + 1][1]);
          ctx.stroke();
          if (idx == 2) {
            ctx.moveTo(
              listCornersPoly[idx + 1][0],
              listCornersPoly[idx + 1][1]
            );
            ctx.lineTo(listCornersPoly[0][0], listCornersPoly[0][1]);
            ctx.stroke();
          }
        }
      });
    }
  }

  onClickEvent(event: MouseEvent) {
    this.checkClickCondition(event);
  }

  calibrateCamera() {
    if (confirm('Deseja iniciar o processo de Calibração?')) {
      alert(
        'Escolha 4 pontos na imagem correspondente ao chão onde ficam as pessoas, formando um polígono de quatro lados. ' +
          'Para isso, comece clicando no ponto do topo esquerdo desse polígono, depois no topo direito, depois na base direita ' +
          'e depois na base esquerda. É necessário obedecer o sentido horário. Caso os pontos escolhidos não estejam bons, ' +
          "clique em 'Resetar Pontos' e comece novamente. Caso contrário, você ficou satisfeito com os pontos, clique em " +
          "'Finalizar Calibração'"
      );
      this.resetRect();
      this.allowedToCalibrate = true;
      let btnResCalib = document.getElementById('btn-reset-calib');
      btnResCalib.hidden = false;
    }
  }

  getImgWidth() {
    return String(this.currentCamera.secondaryWidth + 'px');
  }

  getImgHeight() {
    return String(this.currentCamera.secondaryHeight + 'px');
  }

  getCameraStreamLink() {
    if (
      this.currentLink == undefined ||
      this.currentLink != this.currentCamera.urlSecondary
    ) {
      this.resetRect();
      let btnResCalib = document.getElementById('btn-reset-calib');
      btnResCalib.hidden = true;
      this.allowedToCalibrate = false;
      this.currentLink = this.currentCamera.urlSecondary;
    }
    let btnResCalib = document.getElementById('btn-reset-calib');
    //btnResCalib.style.transform = "translateY(-" + this.getNormalizedHeight() +"px)";
    let btnFinCalib = document.getElementById('btn-final-calib');
    //btnFinCalib.style.transform = "translateY(-" + this.getNormalizedHeight() +"px)";

    return this.currentCamera.urlSecondary;
  }

  getNormalizedWidth() {
    let aspectRatio =
      Number(this.currentCamera.secondaryWidth) /
      Number(this.currentCamera.secondaryHeight);
    if (aspectRatio == 16 / 9) return 640;
    // Normalized to 640/360
    else if (aspectRatio == 4 / 3) return 640;
    // Normalized to 640/480
    else if (aspectRatio == 16 / 10) return 640;
    // Normalized to 640/400
    else if (aspectRatio == 3 / 2) return 720; // Normalized to 720/480

    return 600; // Normalized to 600/600 aspect ratio 1/1
  }

  getNormalizedHeight() {
    let aspectRatio =
      Number(this.currentCamera.secondaryWidth) /
      Number(this.currentCamera.secondaryHeight);
    if (aspectRatio == 16 / 9) return 360;
    // Normalized to 640/360
    else if (aspectRatio == 4 / 3) return 480;
    // Normalized to 640/480
    else if (aspectRatio == 16 / 10) return 400;
    // Normalized to 640/400
    else if (aspectRatio == 3 / 2) return 480; // Normalized to 720/480

    return 600; // Normalized to 600/600 aspect ratio 1/1
  }

  getOriginalPoints() {
    let calibOriginalPoints = [];
    this.listCorners.forEach((coord) => {
      let percentagePointX = coord[0] / this.getNormalizedWidth();
      let percentagePointY = coord[1] / this.getNormalizedHeight();
      let originalPointX =
        percentagePointX * Number(this.currentCamera.secondaryWidth);
      let originalPointY =
        percentagePointY * Number(this.currentCamera.secondaryHeight);
      calibOriginalPoints.push(Math.round(originalPointX));
      calibOriginalPoints.push(Math.round(originalPointY));
    });
    return calibOriginalPoints;
  }

  finalizeCalibration() {
    if (this.listCorners.length == 4) {
      let calibPoints = this.getOriginalPoints();
      this.currentCamera.calibrationPoints = calibPoints;
      // put request

      const body = { calibrationPoints: calibPoints, calibrated: true };
      this.cameraService.save(body, this.currentCamera.id).subscribe({
        next: (data) => {
          alert('Calibração finalizada com Sucesso!');
        },
        error: (error) => {
          alert('Problemas de conexão. Tente novamente mais tarde!');
          console.error('There was an error!', error);
        },
        complete: () => {
          this.calibrationDone.emit(this.currentCamera.id);
        },
      });
    } else {
      console.log('Erro. Reinicie a Calibração!');
    }
  }

  loadSrc() {
    var re = /(?:\.([^.]+))?$/;
    var ext = re.exec(this.getCameraStreamLink())[1];
    if (ext === 'flv') {
      var imgElem = document.getElementById('img-draw');
      imgElem.style.display = 'none';
      var videoElem = document.getElementById('videoElementCalib');
      videoElem.style.display = 'inline';

      if (flvjs.isSupported()) {
        const videoElement = <HTMLAudioElement>(
          document.getElementById('videoElementCalib')
        );
        if (this.flvPlayer != undefined) {
          this.flvPlayer.unload();
          this.flvPlayer.destroy();
          this.flvPlayer = undefined;
          this.flvPlayer = flvjs.createPlayer(
            {
              type: 'flv',
              url: this.getCameraStreamLink(),
            },
            {
              enableStashBuffer: false,
              autoCleanupSourceBuffer: true,
              isLive: true,
            }
          );
          this.flvPlayer.attachMediaElement(videoElement);
          this.flvPlayer.load();
          this.flvPlayer
            .play()
            .then(() => {
              this.drawPrevRect();
              this.isLoaded = true;
            })
            .catch((error) => {
              console.log('Error no Player', error);
              this.flvPlayer.unload();
              this.flvPlayer.load();
              this.flvPlayer.play().then(() => {
                this.drawPrevRect();
                this.isLoaded = true;
              });
            });
          this.flvPlayer.on('error', (e) => console.log(e));
        }
      }
    } else {
      var imgElem = document.getElementById('imgDash');
      imgElem.style.display = 'inline';
      var videoElem = document.getElementById('videoElementCalib');
      videoElem.style.display = 'none';
      imgElem.setAttribute('src', this.getCameraStreamLink());
    }
  }

  ngOnChanges() {
    this.isLoaded = false;
    this.loadSrc();
  }

  ngAfterViewInit() {
    // this.loadSrc();
  }

  ngOnInit() {}
}
