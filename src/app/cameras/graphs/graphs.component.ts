import { Component, OnInit, ViewChild, ElementRef, SimpleChanges, Input } from '@angular/core';
import { RecordModel } from "../../_models";
import * as Plotly from 'plotly.js/dist/plotly.js';


@Component({
  selector: 'app-graphs',
  templateUrl: './graphs.component.html',
  styleUrls: ['./graphs.component.scss']
})
export class GraphsComponent implements OnInit {
  public data: any;
  dts: any;
  riskLayout: any;
  riskLayoutPie: any;
  riskLayoutMask: any;
  riskLayoutCrowd: any;
  /* The plot target container. */
  @ViewChild('plotContainer') plotContainer: ElementRef;
  @ViewChild('plotContainerPie') plotContainerPie: ElementRef;
  @ViewChild('plotContainerMask') plotContainerMask: ElementRef;
  @ViewChild('plotContainerCrowd') plotContainerCrowd: ElementRef;

  @Input() recordData: RecordModel[];
  constructor() { }

  ngOnInit() { }

  ngAfterViewInit() {
    this.initPlot();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initPlot();
    if (changes && changes.data && changes.data.previousValue) {
      this.initPlot();
    }

    if (changes && changes.layout && changes.layout.previousValue) {
      this.initPlot();
    }
  }

  ngOnDestroy() {
    // if (this._theme$) { this._theme$.unsubscribe(); }
  }

  private getRiskCount() {
    let risk0 = 0, risk1 = 0, risk2 = 0, risk3 = 0;
    this.recordData.forEach(r => {
      switch (r.severityLevel) {
        case 0 : risk0 ++; break;
        case 1 : risk1 ++; break;
        case 2 : risk2 ++; break;
        case 3 : risk3 ++; break;
      }
    });
    return [risk0, risk1, risk2, risk3]
  }

  private initPlot() {
    this.dts = this.recordData.map(r=>r.createdAt);

    this.riskLayout = {
      autosize: true,
      // height: 500,
      width: 700,
      legend: { orientation: "h", x: 0, y: -0.2 },
      // margin: { l: 10, r: 10, b: 290, t: 70, pad: 0 },
      paper_bgcolor:'rgba(0,0,0,0)',
      plot_bgcolor:'rgba(0,0,0,0)',
      showlegend: true,
      title: { text: "Níveis de Risco" },
      xaxis: {
        autorange: true,
        range: [this.dts[0], this.dts[-1]],
      },
      yaxis: {
        title: "risco",
        nticks: 4,
        autorange: false,
        range: [0, 3],
      }
    }

    this.riskLayoutPie = {
      autosize: true,
      height: 350,
      width: 350,
      legend: { orientation: "h", x: 0, y: -0.2 },
      // margin: { l: 10, r: 10, b: 290, t: 70, pad: 0 },
      paper_bgcolor:'rgba(0,0,0,0)',
      plot_bgcolor:'rgba(0,0,0,0)',
      showlegend: true,
      title: { text: "Níveis de Risco" },
      xaxis: {
        autorange: true,
        range: [this.dts[0], this.dts[-1]],
      },
      yaxis: {
        title: "risco",
        nticks: 4,
        autorange: false,
        range: [0, 3],
      }
    }

    this.riskLayoutMask = {
      autosize: true,
      // height: 500,
      width: 900,
      legend: { orientation: "h", x: 0, y: -0.2 },
      // margin: { l: 10, r: 10, b: 290, t: 70, pad: 0 },
      paper_bgcolor:'rgba(0,0,0,0)',
      plot_bgcolor:'rgba(0,0,0,0)',
      showlegend: true,
      title: { text: "Uso de Máscaras" },
      xaxis: {
        autorange: true,
        range: [this.dts[0], this.dts[-1]],
      },
      yaxis: {
        title: "Quantidade",
        nticks: 4,
        autorange: true,
      }
    }

    this.riskLayoutCrowd = {
      autosize: true,
      // height: 500,
      width: 900,
      legend: { orientation: "h", x: 0, y: -0.2 },
      // margin: { l: 10, r: 10, b: 290, t: 70, pad: 0 },
      paper_bgcolor:'rgba(0,0,0,0)',
      plot_bgcolor:'rgba(0,0,0,0)',
      showlegend: true,
      title: { text: "Aglomeração" },
      xaxis: {
        autorange: true,
        range: [this.dts[0], this.dts[-1]],
      },
      yaxis: {
        title: "Quantidade",
        nticks: 4,
        autorange: true,
      }
    }

    this.data = [
      {
        x: this.dts,
        y: this.recordData.map(r=>r.severityLevel),
        // type: 'scatter',
        type: 'bar',
        // fill: "none",
        // mode: "lines+markers",
        // line: { shape: 'linear', width: 1 },
        name: "Severidade",
        hovermode: 'closest',
        hoverlabel: { namelength: -1 },
        xaxis: 'x',
        yaxis: 'y'
      },
      {
        values: this.getRiskCount(),
        type: 'pie',
        labels:['baixo (0)', 'médio (1)', 'alto (2)', 'altíssimo (3)']
      },
      {
        x: this.dts,
        y: this.recordData.map(r=>r.peopleDetected),
        type: 'scatter',
        fill: "none",
        mode: "lines+markers",
        line: { shape: 'linear', width: 1 },
        name: "Pessoas Detectadas",
        xaxis: 'x',
        yaxis: 'y'
      },
      {
        x: this.dts,
        y: this.recordData.map(r=>r.crowdNumber),
        type: 'scatter',
        fill: "none",
        mode: "lines+markers",
        line: { shape: 'linear', width: 1 },
        name: "Aglomerações",
        xaxis: 'x',
        yaxis: 'y'
      },
      {
        x: this.dts,
        y: this.recordData.map(r=>r.facesDetected),
        type: 'scatter',
        fill: "none",
        mode: "lines+markers",
        line: { shape: 'linear', width: 1 },
        name: "Rostos Detectados",
        xaxis: 'x',
        yaxis: 'y'
      },
      {
        x: this.dts,
        y: this.recordData.map(r=>r.noMaskNumber),
        type: 'scatter',
        fill: "none",
        mode: "lines+markers",
        line: { shape: 'linear', width: 1 },
        name: "Rostos sem Máscara",
        xaxis: 'x',
        yaxis: 'y'
      }
    ]

    if (this.data !== undefined) {
      Plotly.newPlot(this.plotContainer.nativeElement, [this.data[0]], this.riskLayout, { staticPlot: false });
      Plotly.newPlot(this.plotContainerPie.nativeElement, [this.data[1]], this.riskLayoutPie, { staticPlot: false });
      Plotly.newPlot(this.plotContainerMask.nativeElement, [this.data[4], this.data[5]], this.riskLayoutMask, { staticPlot: false });
      Plotly.newPlot(this.plotContainerCrowd.nativeElement, [this.data[2], this.data[3]], this.riskLayoutCrowd, { staticPlot: false });
    } else {
      console.warn('The data or the layout are not defined');
    }
  }

  /** On resize this method triggers & resize the plot. */
  public onResize() {
    Plotly.Plots.resize(this.plotContainer.nativeElement);
  }
}