import { Component, OnInit, OnChanges, Input, AfterViewInit, Output, EventEmitter } from "@angular/core";
import { AuthenticationService, RecordService, CameraService } from "../../_services";
import { CameraModel, RecordModel } from "../../_models";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import flvjs from 'flv.js';

@Component({
  selector: 'app-camera-info',
  templateUrl: './camera-info.component.html',
  styleUrls: ['./camera-info.component.scss'],
})
export class CameraInfoComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() currentCamera: CameraModel;
  @Output() dashUpdateCamera = new EventEmitter<CameraModel>();
  user: any;
  cameraRecords: RecordModel[] = [];
  x: number;
  y: number;
  listCorners = [];
  currentLink = undefined;
  fullScreen = false;
  streamElem = 'imgDash';
  flvPlayer = undefined;
  isLoaded = false;

  constructor(
    public dialog: MatDialog,
    private _authService: AuthenticationService,
    private _recordService: RecordService,
    private _cameraService: CameraService,
    private _snackBar: MatSnackBar
  ) {
    this.user = this._authService.currentUser;
  }

  getCameraStreamLink() {
    if (
      this.currentLink == undefined ||
      this.currentLink != this.currentCamera.urlSecondary
    ) {
      this.currentLink = this.currentCamera.urlSecondary;
    }

    return this.currentCamera.urlSecondary;
  }

  getNormalizedWidth() {
    let aspectRatio = this.currentCamera.secondaryWidth / this.currentCamera.secondaryHeight;

    // if(aspectRatio == 16/9)
    //   return 640; // Normalized to 640/432
    // else if(aspectRatio == 4/3)
    //   return 640; // Normalized to 640/480
    // else if(aspectRatio == 16/10)
    //   return 640 // Normalized to 640/400

    return 640; // Normalized to 600/600 aspect ratio 1/1
  }

  getNormalizedHeight() {
    let aspectRatio = this.currentCamera.secondaryWidth / this.currentCamera.secondaryHeight;

    // if(aspectRatio == 16/9)
    //   return 360; // Normalized to 768/432
    // else if(aspectRatio == 4/3)
    //   return 480; // Normalized to 640/480
    // else if(aspectRatio == 16/10)
    //   return 400; // Normalized to 640/400

    return 360; // Normalized to 600/600 aspect ratio 1/1
  }

  closefullscreen() {
    const docWithBrowsersExitFunctions = document as Document & {
      mozCancelFullScreen(): Promise<void>;
      webkitExitFullscreen(): Promise<void>;
      msExitFullscreen(): Promise<void>;
    };
    if (docWithBrowsersExitFunctions.exitFullscreen) {
      docWithBrowsersExitFunctions.exitFullscreen();
    } else if (docWithBrowsersExitFunctions.mozCancelFullScreen) {
      /* Firefox */
      docWithBrowsersExitFunctions.mozCancelFullScreen();
    } else if (docWithBrowsersExitFunctions.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      docWithBrowsersExitFunctions.webkitExitFullscreen();
    } else if (docWithBrowsersExitFunctions.msExitFullscreen) {
      /* IE/Edge */
      docWithBrowsersExitFunctions.msExitFullscreen();
    }
  }

  setCameraActive(active: boolean) {
    this._cameraService
      .updateCamera(this.currentCamera.id, { active: active })
      .subscribe(
        (res) => {
          this.dashUpdateCamera.emit(res);
          this.currentCamera = res;
          var message = active ? 'Câmera ativada' : 'Câmera desativada';
          this._snackBar.open(message, 'ok', {
            duration: 2000,
            horizontalPosition: 'center',
            verticalPosition: 'bottom',
          });
        },
        (err) => {
          console.warn(err);
          this._snackBar.open('Erro', 'ok', {
            duration: 2000,
            horizontalPosition: 'center',
            verticalPosition: 'bottom',
          });
        }
      );
  }

  toggleFullScreen() {
    var elem = document.getElementById(this.streamElem);

    if (this.fullScreen == false) {
      if (elem.requestFullscreen) {
        elem.requestFullscreen();
      }
      this.fullScreen = true;
    } else {
      this.closefullscreen();
      this.fullScreen = false;
    }
  }

  async loadSrc() {
    var re = /(?:\.([^.]+))?$/;
    var ext = re.exec(this.getCameraStreamLink())[1];
    if (ext === "flv") {
      var imgelem = document.getElementById('imgDash');
      imgelem.style.display = 'none';
      var videoelem = document.getElementById('videoElement');
      videoelem.style.display = 'inline';
      this.streamElem = 'videoElement';

      if (flvjs.isSupported()) {
        const videoElement = <HTMLAudioElement>document.getElementById('videoElement');
        if (this.flvPlayer != undefined) {
          this.flvPlayer.unload();
          this.flvPlayer.destroy();
          this.flvPlayer = undefined;
        }
        this.flvPlayer = flvjs.createPlayer({
          type: 'flv',
          url: this.getCameraStreamLink()
        }, {
          enableStashBuffer: false,
          autoCleanupSourceBuffer: true,
          isLive: true
        });
        this.flvPlayer.attachMediaElement(videoElement);
        this.flvPlayer.load();
        this.flvPlayer.play().then(() => {
          this.isLoaded = true;
        });
        
      }
    } else {
      var imgElem = document.getElementById('imgDash');
      imgElem.style.display = 'inline';
      var videoElem = document.getElementById('videoElement');
      videoElem.style.display = 'none';
      imgElem.setAttribute('src', this.getCameraStreamLink());
      this.isLoaded = true;
    }
  }

  ngOnInit() {
    function openFullscreen() {
      var elem = document.getElementById(this.streamElem);
      elem.requestFullscreen();
    }
  }

  ngOnChanges() {
    this.isLoaded = false;
    this.loadSrc();
    if (this.currentCamera && this.currentCamera.id) {
      this._recordService
        .getCameraRecords(this.currentCamera.id)
        .subscribe((res) => {
          this.cameraRecords = res.rows;
        });
    }
  }

  ngAfterViewInit(){
    // this.loadSrc();
  }

  ngOnDestroy() { }
}
