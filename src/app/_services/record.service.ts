import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class RecordService {
  constructor(private http: HttpClient) {}

  getAllRecords(cameraId: String) {
    return this.http.get<any[]>(`${environment.baseUrl}/api/records/${cameraId}`);
  }

  getCameraRecords(cameraId: string) {
    return this.http.get<any>(`${environment.baseUrl}/api/records/camera/${cameraId}`);
  }
}