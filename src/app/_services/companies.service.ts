import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { DefaultService } from './default.service'
import { CompanyModel } from "../_models";

@Injectable({ providedIn: 'root' })
export class CompaniesService extends DefaultService {
  constructor(private _http: HttpClient) {
    super(_http, 'companies');
  }

  getAllCompanies() {
    return this._http.get(`${environment.baseUrl}/api/companies?active=true&limit=100`);
  }

  getCompany(companyId: string){
    return this._http.get(`${environment.baseUrl}/api/companies/${companyId}`);
  }

  postCompany(body: CompanyModel) {
    return this._http.post(`${environment.baseUrl}/api/companies/`, body);
  }

  updateCompany(companyId: string, body: CompanyModel) {
    return this._http.post(`${environment.baseUrl}/api/companies/${companyId}`, body);  
  }
}
