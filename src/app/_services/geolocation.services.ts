import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class GeolocationService {
  constructor(private http: HttpClient) {}

  getCurrentLocation() {
    return this.http.get("https://geolocation-db.com/json/");
  }
}