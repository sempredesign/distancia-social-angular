import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { UserModel } from '../_models';
import { DefaultService } from './default.service'

@Injectable({ providedIn: 'root' })
export class UserService extends DefaultService {
  constructor(private _http: HttpClient) {
    super(_http, 'users');
  }

  getAll() {
    return this._http.get<UserModel[]>(`${environment.baseUrl}/api/users`);
  }
}
