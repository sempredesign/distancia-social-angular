import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

export class DefaultService {
  _baseComponent: string;
  _baseUrl: string;
  constructor(private http: HttpClient, baseComponent: string) {
    this._baseComponent = baseComponent;
    this._baseUrl = environment.baseUrl + '/api/';
  }

  get(id: string) {
    let url = this._baseUrl + this._baseComponent + '/' + id;
    return this.http.get(url);
  }

  getList(
    limit?: number,
    page?: number,
    query?: string,
    customUrlParams?: Array<any>,
    sort?: string
  ) {
    let url =
      this._baseUrl +
      this._baseComponent +
      (limit || page || query || customUrlParams || sort ? '?' : '') +
      (query ? 'q=' + query : '') +
      (limit ? '&limit=' + limit : '') +
      (page ? '&page=' + page : '') +
      (sort ? '&sort=' + sort : '');

    if (customUrlParams) {
      customUrlParams.forEach((param) => {
        url += '&' + param.name + '=' + param.value;
      });
    }
    return this.http.get(url);
  }

  save(obj: any, id?: string) {
    if (!id) return this.http.post(this._baseUrl + this._baseComponent, obj);
    return this.http.put(this._baseUrl + this._baseComponent + '/' + id, obj);
  }
  
  remove(id: string) {
    return this.http.delete(this._baseUrl + this._baseComponent + '/' + id);
  }
}
