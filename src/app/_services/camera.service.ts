import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { DefaultService } from './default.service'
import { CameraModel } from '../_models';

@Injectable({ providedIn: 'root' })
export class CameraService extends DefaultService {
  constructor(private _http: HttpClient) {
    super(_http, 'cameras');
  }

  getAllCameras() {
    return this._http.get<any[]>(`${environment.baseUrl}/api/cameras?active=true&limit=100`);
  }

  getCamera(cameraId: string) {
    return this._http.get<any>(`${environment.baseUrl}/api/camera/${cameraId}`);
  }

  postCamera(body: CameraModel) {
    return this._http.post<any>(`${environment.baseUrl}/api/cameras/`, body);
  }

  updateCamera(cameraId: string, body: CameraModel) {
    return this._http.put<any>(`${environment.baseUrl}/api/cameras/${cameraId}`, body);
  }
}
