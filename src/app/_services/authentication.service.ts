﻿﻿import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  userReady: EventEmitter<boolean>;

  constructor(
    // private router: Router,
    private http: HttpClient,
    private jwtHelper: JwtHelperService
  ) {
    this.userReady = new EventEmitter();
  }

  changeCompany(companyId) {
    return this.http
      .get<any>(`${environment.baseUrl}/api/auth/changecompany/${companyId}`)
      .pipe(
        map((res) => {
          if (res && res.token) {
            localStorage.setItem('token', res.token);
            this.userReady.emit(true);
            window.location.reload();
            return true;
          }
          return false;
        })
      );
  }

  login(username: string, password: string) {
    return this.http
      .post<any>(
        `${environment.baseUrl}/api/auth/local`,
        { username, password },
        { withCredentials: true }
      )
      .pipe(
        map((res) => {
          if (res && res.token) {
            localStorage.setItem('token', res.token);
            this.userReady.emit(true);
            return true;
          }
          return false;
        })
      );
  }

  logout() {
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    this.userReady.emit(false);
    // this.router.navigate(['/login']);
  }

  isLoggedIn() {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
    // const user = localStorage.getItem('user');
    // return (
    //   user != null && token != null && !this.jwtHelper.isTokenExpired(token)
    // );
  }

  get currentUser() {
    const token = localStorage.getItem('token');
    if (!token) return null;
    const user = this.jwtHelper.decodeToken(token);
    return user;
  }
}
