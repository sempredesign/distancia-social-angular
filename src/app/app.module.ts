import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

import { MatBadgeModule } from '@angular/material/badge'; 
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule} from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard, JwtInterceptor, ErrorInterceptor } from './_helpers';
import { CamerasComponent } from './cameras/cameras.component';
import { LoginComponent } from './login/login.component';
import { RegistersComponent } from './registers/registers.component';
import { MapComponent } from './cameras/map/map.component';
import { GraphsComponent } from './cameras/graphs/graphs.component';
import { CalibrationComponent } from './cameras/calibration/calibration.component';
import { DialogContentMap } from './_shared/map-selector.component';
import { DialogContentFilter } from './_shared/filter-selector.component';
import { ConfirmDialogComponent } from './_shared/confirm-dialog.component';
import { CameraInfoComponent } from './cameras/camera-info/camera-info.component';
import { RegisterCameraComponent } from './registers/register-camera/register-camera.component';
import { RegisterUserComponent } from './registers/register-user/register-user.component';
import { RegisterCompanyComponent } from './registers/register-company/register-company.component';
import { NewUserComponent } from './registers/register-user/new-user/new-user.component';
import { NewCameraComponent } from './registers/register-camera/new-camera/new-camera.component';
import { NewCompanyComponent } from './registers/register-company/new-company/new-company.component';
//import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// import 'web-component-essentials';
import './components/nav-menu';
import './components/user-menu';
import './components/help-menu';
import './components/logo-motora';
import { AboutComponent } from './about/about.component';
import { FaqComponent } from './faq/faq.component';
import { SetupCamerasComponent } from './setup-cameras/setup-cameras.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CamerasComponent,
    RegistersComponent,
    MapComponent,
    GraphsComponent,
    CalibrationComponent,
    DialogContentMap,
    DialogContentFilter,
    ConfirmDialogComponent,
    CameraInfoComponent,
    RegisterCameraComponent,
    RegisterUserComponent,
    RegisterCompanyComponent,
    NewUserComponent,
    NewCameraComponent,
    NewCompanyComponent,
    AboutComponent,
    FaqComponent,
    SetupCamerasComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatCheckboxModule,
    MatButtonModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatBadgeModule,
    LeafletModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('token');
        },
      },
    }),
    //NgbModule,
  ],
  providers: [
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
  entryComponents: [DialogContentMap, DialogContentFilter, NewUserComponent, ConfirmDialogComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA // Tells Angular we will have custom tags in our templates
  ]
})
export class AppModule {}
