import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupCamerasComponent } from './setup-cameras.component';

describe('SetupCamerasComponent', () => {
  let component: SetupCamerasComponent;
  let fixture: ComponentFixture<SetupCamerasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetupCamerasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupCamerasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
