const template = document.createElement('template');
template.innerHTML = `
<div class="cap">
    <div class="user-info">
        <span class="name"></span>
        <span class="role"></span>
    </div>
    <button>
        <span>AB</span>
    </button>
</div>
<div class="curtain">
    <div class="account">
        <span>Minha conta:</span>
        <ul>
            <li>
                <a href="#my-account">Minha conta</a>
            </li>
            <li>
                <a href="logout">Sair</a>
            </li>
        </ul>
    </div>
    <div class="companies">
        <span>Administrar instituição:</span>
        <ul>
            <li>
                <a href="">Motora</a>
            </li>
        </ul>
    </div>
</div>
`;

class UserMenu extends HTMLElement {

    constructor() {
        super();
        this._visible = this.getAttribute("visible");
        this._hidden = this.getAttribute("hidden");
        this._name = this.getAttribute("name");
        this._role = this.getAttribute("role");
    }
    
    get visible() { return this._visible; }     
    set visible(val) { this.setAttribute('visible', val); }
     
    get hidden() { return this._hidden; }     
    set hidden(val) { this.setAttribute('hidden', val); }
     
    get name() { return this._name; }     
    set name(val) { this.setAttribute('name', val); }
     
    get role() { return this._role; }     
    set role(val) { this.setAttribute('role', val); }
     
    get companies() { return this._companies; }     
    set companies(val) { this._companies = val; this.renderCompanies(); }
     
    static get observedAttributes() { 
        return ['visible', 'name', 'role']; 
    }
    
    attributeChangedCallback(name, oldVal, newVal) {
        switch(name) {
            case 'visible':
                this._visible = newVal === "true";
                this.toggleMenu();
                break;
            case 'name':
                this._name = newVal;
                this.writeName();
                break;
            case 'role':
                this._role = newVal;
                this.writeName();
                break;
        }
    }
    
    connectedCallback () {
        this.appendChild(template.content.cloneNode(true));
        var button = this.querySelector(".cap button");
        button.addEventListener("click", (e) => {
            this.setAttribute("visible", !this._visible);
        });
        button.addEventListener("blur", (e) => {
            window.setTimeout(() => {
                this.setAttribute("visible", false);
            }, 250);
        });
        this.querySelector("a[href='logout']").addEventListener("click", (e) => {
            this.dispatchEvent(new CustomEvent('logout'));
            e.preventDefault();
            return false;
        })
        this.toggleMenu();
        this.renderCompanies();
    }
    
    writeName() {
        this.querySelector(".name").innerHTML = this._name;
        this.querySelector(".role").innerHTML = this._role;
    }
    
    toggleMenu() {
        if (this.classList) {
            this._visible ? this.classList.add('visible') : this.classList.remove('visible');
        }
    }

    renderCompanies() {
        const companiesElement = this.querySelector(".curtain .companies ul");
        const ulElement = companiesElement.querySelector("ul");
        companiesElement.innerHTML = "";
        this.companies && this.companies.forEach(o => {
            let item = document.createElement("li");
            item.innerHTML = `
                <a href="/company/${o.name}">${o.name}</a>
            `
            item.querySelector("a").addEventListener("click", (e) => {
                this.dispatchEvent(new CustomEvent('changeCompany', { detail: o }));
                e.preventDefault();
                return false;
            })
            companiesElement.appendChild(item);
        });
    }
    
}

customElements.define('user-menu', UserMenu);
export default UserMenu;