const template = document.createElement('template');
template.innerHTML = `
<div class="cap">
    <button>
        <span>Ir para...</span>
    </button>
</div>

<div class="curtain">
    <ul>
        <li class="events">
            <a href="/">
                <span class="icon"></span>
                <span>Monitoramento</span>
            </a>
        </li>
        <li class="cameras">
            <a href="/registers">
                <span class="icon"></span>
                <span>Cadastros</span>
            </a>
        </li>
    </ul>
</div>
`;

class NavMenu extends HTMLElement {

    constructor() {
        super();
        this._visible = this.getAttribute("visible");
        this._selecteditem = this.getAttribute("selecteditem");
        // this.goto = new CustomEvent('goto');
    }
    
    get visible() {
        return this._visible;
    }
    set visible(val) {
        this.setAttribute('visible', val);
    }
     
    get selecteditem() {
        return this._selecteditem;
    }
    set selecteditem(val) {
        this.setAttribute('selecteditem', val);
    }
    
     
    get goto() {
        return this._goto;
    }
    set goto(val) {
        this.setAttribute('goto', val);
    }
    
    static get observedAttributes() { 
        return ['visible', 'selecteditem', 'goto']; 
    }
    
    attributeChangedCallback(name, oldVal, newVal) {
        switch(name) {
            case 'visible':
                this._visible = newVal === "true" || false;
                this.changeMenu();
                break;
            case "selecteditem":
                this._selecteditem = newVal;
                this.selectItem();
                break;
            case "goto":
                this._goto = newVal;
                this.goto();
                break;
        }
    }
    
    connectedCallback () {
        this.appendChild(template.content.cloneNode(true));

        var button = this.querySelector("button");
        button.addEventListener("click", (e) => {
            this.setAttribute("visible", !this._visible);
        });
        button.addEventListener("blur", (e) => { 
            window.setTimeout(() => {
                this.setAttribute("visible", false);
            }, 250);
        });
        this.selectItem();
    }
    
    changeMenu() {
        if (this.classList) {
            this._visible ? this.classList.add('visible') : this.classList.remove('visible');
        }
    }

    selectItem() {
        var items = this.querySelectorAll("li");
        items.forEach(o => { 
            o.classList.remove("selected");
            let a = o.querySelector("a");
            let href = a.getAttribute("href");
            a.addEventListener("click", (e) => {
                console.log(href);
                this.dispatchEvent(new CustomEvent('goto', { detail: href }));
                e.preventDefault();
                return false;
            });
            if (href === this._selecteditem) {
                o.classList.add("selected");
            }
        });
    }
    
}

customElements.define('nav-menu', NavMenu);
export default NavMenu;
