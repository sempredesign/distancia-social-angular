const template = document.createElement('template');
template.innerHTML = `
<div class="cap">
    <button>
        <span>Ajuda</span>
    </button>
</div>

<div class="curtain">
    <ul>
        <li class="faq">
            <a href="faq">
                <span>Perguntas e respostas</span>
            </a>
        </li>
        <li class="setup-cameras">
            <a href="setup-cameras">
                <span>Configurando câmeras</span>
            </a>
        </li>
        <li class="about">
            <a href="about">
                <span>Sobre este software</span>
            </a>
        </li>
    </ul>
</div>
`;

class HelpMenu extends HTMLElement {

    constructor() {
        super();
        this._visible = this.getAttribute("visible") === "true";
    }
    
    get visible() {
        return this._visible;
    }
     
    set visible(val) {
        this.setAttribute('visible', val);
    }
     
    static get observedAttributes() { 
        return ['visible']; 
    }
    
    attributeChangedCallback(name, oldVal, newVal) {
        switch(name) {
            case 'visible':
                this._visible = newVal === "true";
                this.toggleMenu();
                break;
        }
    }
    
    connectedCallback () {
        this.appendChild(template.content.cloneNode(true));
        var button = this.querySelector(".cap button");
        button.addEventListener("click", (e) => {
            this.setAttribute("visible", !this._visible);
        });
        button.addEventListener("blur", (e) => {
            window.setTimeout(() => {
                this.setAttribute("visible", false);
            }, 250);
        });
        this.toggleMenu();
        var items = this.querySelectorAll(".curtain li");
        items.forEach(o => { 
            let a = o.querySelector("a");
            let href = a.getAttribute("href");
            a.addEventListener("click", (e) => {
                this.dispatchEvent(new CustomEvent('goto', { detail: href }));
                e.preventDefault();
                return false;
            });
        });
    }
    
    toggleMenu() {
        if (this.classList) {
            this._visible ? this.classList.add('visible') : this.classList.remove('visible');
        }
    }
    
}

customElements.define('help-menu', HelpMenu);
export default HelpMenu;
