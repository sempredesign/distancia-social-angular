const template = document.createElement('template');
template.innerHTML = `
<div class="tools">
    <ul class="tags">
        <li>
            <a href="active">Ativos</a>
        </li>
        <li>
            <a href="locked">Bloqueados</a>
        </li>
    </ul>
    <button class="add">
        <span>
            Adicionar usuário
        </span>  
    </button>
</div>

<table>
    <thead>
        <tr>
            <th></th>
            <th>Nome</th>
            <th>Login</th>
            <th>E-mail</th>
            <th>Perfil</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <a href="">Amanda Vicente Pereira</a>
            </td>
            <td>
                <a href="">amanda</a>
            </td>
            <td>
                <a href="">amanda@companhialtda.com.br</a>
            </td>
            <td>
                <a href="">Administrador</a>
            </td>
        </tr>
    </tbody>
</table>

<div class="navigation">
    <div class="current">
        Página 
        <span>1</span>
        de 
        <span>5</span>
    </div>
    <div class="count">
        <span class="filter">
            Filtrados: <span>22</span>
        </span>
        <span class="total">
            Total: <span>26</span> uuários
        </span>
    </div>
    <div class="pages">
        <span>1</span>
        <a href="#page2">2</a>
        <a href="#page3">3</a>
        <a href="#page4">4</a>
        <a href="#page5">5</a>
    </div>
</div>
`;

class TableData extends HTMLElement {

    constructor() {
        super();
        this._selectedTags = this.getAttribute("selectedtags");
    }
    
    get content() {
        return this._content;
    }
    
    set content(val) {
        this._content = val;
        this.renderRows();
    }

    static get observedAttributes() { 
        return ['selectedtags']; 
    }
    
    attributeChangedCallback(name, oldVal, newVal) {
        switch(name) {
            case "selectedtags":
                this._selectedTags = newVal.split(' ');
                this.selectTags();
        }
    }
    
    connectedCallback () {
        this.appendChild(template.content.cloneNode(true));
        new ImportComponent("table-data", (o) => {
            this.innerHTML = o.target.responseText;

            this.selectTags();
            var a = this.querySelectorAll(".tags a");
            a.forEach(o => {
                o.addEventListener("click", (e) => {
                    //add or remove the tag attribute
                    let tag = o.href.split("/").slice(-1)[0];
                    let index = this._selectedTags.indexOf(tag);
                    if (index >= 0) {
                        this._selectedTags.splice(index, 1);
                    } else {
                        this._selectedTags.splice(index, 0, tag);
                    }
                    this.setAttribute("selectedtags", this._selectedTags.join(" "));
                    e.preventDefault();
                    return false;
                });
            });
        });
    }
    
    selectTags() {
        var items = this.querySelectorAll(".tags li");
        items.forEach(o => {
            o.classList.remove("selected");
            let tag = o.querySelector("a").href.split("/").slice(-1)[0];
            this._selectedTags.forEach(t => {
                if (tag === t) {
                    o.classList.add("selected");
                }
            });
        });
    }

    renderRows() {
        let tbody = this.querySelector("tbody");
        let rows = [];
        this._content.forEach((o) => {
            rows.push("<tr>" +
            "<td class='" + (o.locked ? "locked" : "ok") + "'></td>" +
                    "<td><a href='"+o.login+"'>" + o.name + "</a></td>" +
                    "<td><a href='"+o.login+"'>" + o.login + "</a></td>" +
                    "<td><a href='"+o.login+"'>" + o.email + "</a></td>" +
                    "<td><a href='"+o.login+"'>" + o.role + "</a></td>" +
                "</tr>");
        })
        
        tbody.innerHTML = rows.join("\n");
    }
    
}

customElements.define('table-data', TableData);
export default TableData;
