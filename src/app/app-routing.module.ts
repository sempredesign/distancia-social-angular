import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component'
import { CamerasComponent } from './cameras/cameras.component';
import { RegistersComponent } from './registers/registers.component'
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './_helpers';
import { AboutComponent } from './about/about.component';
import { FaqComponent } from './faq/faq.component';
import { SetupCamerasComponent } from './setup-cameras/setup-cameras.component';


const routes: Routes = [
    { path: '', component: CamerasComponent, canActivate: [AuthGuard] },
    { path: 'registers', component: RegistersComponent, canActivate: [AuthGuard] },
    { path: 'about', component: AboutComponent },
    { path: 'faq', component: FaqComponent },
    { path: 'setup-cameras', component: SetupCamerasComponent },
    { path: 'login', component: LoginComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }