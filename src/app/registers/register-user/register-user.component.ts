import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { UserModel } from '../../_models';
import { UserService } from '../../_services';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { NewUserComponent } from './new-user/new-user.component';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss'],
})
export class RegisterUserComponent implements AfterViewInit {
  displayedColumns: string[] = ['name', 'email', 'role', 'companyName'];
  columnLabels = {
    'name': 'Nome',
    'email': 'Email',
    'role': 'Função',
    'companyName': 'Instituição'
  }
  usersCompany: UserModel[];
  dataSource: MatTableDataSource<UserModel>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private userService: UserService, public dialog: MatDialog) {}

  ngAfterViewInit(): void {
    this.reloadUsers();
  }

  reloadUsers() {
    this.userService.getAll().subscribe(
      (response) => {
        this.usersCompany = response['rows'].map((user: UserModel) => {
          return { ...user, companyName: user.company.name };
        });
        this.dataSource = new MatTableDataSource(this.usersCompany);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  editUser(user: UserModel) {
    const dialogRef = this.dialog.open(NewUserComponent, {
      data: {user},
      width: '25em',
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.reloadUsers();
    });
  }

  newUser() {
    const dialogRef = this.dialog.open(NewUserComponent, {
      data: {},
      width: '25em'
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.reloadUsers();
    });
  }
}
