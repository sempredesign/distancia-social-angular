import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService, CompaniesService, AuthenticationService } from '../../../_services';
import { CompanyModel, UserModel } from '../../../_models';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  ConfirmDialogComponent,
  ConfirmDialogModel,
} from '../../../_shared/confirm-dialog.component';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss'],
})
export class NewUserComponent implements OnInit {
  userForm: any;
  showErrors = false;
  roles = ['user', 'manager', 'admin'];
  rolesLabels = { user: 'Usuário', manager: 'Gerente', admin: 'Administrador' };
  emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  companies: CompanyModel[];
  currentUser: UserModel;
  userRole: string = 'user';
  mode: string = 'create'; // or 'edit'
  title = 'Cadastrar Novo Usuário';
  isLoaded: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private userService: UserService,
    private companiesService: CompaniesService,
    private authService: AuthenticationService,
    private dialogRef: MatDialogRef<NewUserComponent>
  ) {

  }

  ngOnInit(): void {
    if (this.data.user) {
      this.mode = 'edit';
      this.title = 'Editar Usuário'
    } else {
      this.mode = 'create';
      this.title = 'Cadastrar Novo Usuário'
    }

    this.currentUser = this.authService.currentUser;
    this.userRole = this.currentUser.role;
    this.mode = this.data.user ? 'edit' : 'create';

    if (this.currentUser.role === 'admin') {
      this.companiesService.getAllCompanies().subscribe(
        (res) => {
          this.companies = res['rows'];
        },
        (err) => {
          console.warn('companies loading error', err);
        },
        () => {
          // this._loadingService.done();
        }
      );
    }

    this.userForm = this.formBuilder.group({
      name: new FormControl(this.mode === 'edit' ? this.data.user.name : '', [
        Validators.required,
        Validators.maxLength(32),
      ]),
      email: new FormControl(this.mode === 'edit' ? this.data.user.email : '', [
        Validators.required,
        Validators.pattern(this.emailRegex),
      ]),
      role: new FormControl(this.mode === 'edit' ? this.data.user.role : '', [
        Validators.required,
      ]),
      company: new FormControl('', [Validators.required]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(32),
      ]),
    });

    this.company.setValue(this.currentUser.company.id);
    this.isLoaded = true;
  }

  onSubmit() {
    this.showErrors = true;
    this.isLoaded = false;
    this.userService
      .save(this.userForm.value, this.mode === 'edit' ? this.data.user.id : null)
      .subscribe(
        (res) => {
          this.snackBar.open(
            `Usuário ${this.mode === 'edit' ? 'Editado' : 'Cadastrado'}`,
            'ok',
            {
              duration: 2000,
              horizontalPosition: 'center',
              verticalPosition: 'bottom',
            }
          );
        },
        (err) => {
          this.snackBar.open(
            `Erro ao ${this.mode === 'edit' ? 'editar' : 'cadastrar'} usuário`,
            'ok',
            {
              duration: 2000,
              horizontalPosition: 'center',
              verticalPosition: 'bottom',
            }
          );
          console.warn('post error', err);
        },
        () => {
          this.isLoaded = true;
          this.dialogRef.close();
        }
      );
  }

  changeRole(e) {
    this.role.setValue(e.target.value, {
      onlySelf: true,
    });
  }

  getErrorMessage(e) {
    if (!e) return;
    if (e.required)
      return 'Campo necessário';
    if (e.mminlength)
      return `Use pelo menos ${this.name.errors.mminlength.requiredLength} carateres`;
    if (e.maxlength)
      return `Use até ${this.name.errors.maxlength.requiredLength} carateres`;
    return 'Entrada inválida';
  }

  canDelete() {
    if (this.userRole === 'admin') {
      if (this.data.user.role === 'admin')
        return false;
      else return true;
    }

    if (this.userRole === 'manager') {
      if (this.data.user.role === 'user')
        return true
    }

    return false;
  }

  deleteUser() {
    const message = `Deseja remover este usuário?`;
    const dialogData = new ConfirmDialogModel('Confirmar Ação', message);
    const dialogRefConfirm = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '400px',
      data: dialogData,
    });
    dialogRefConfirm.afterClosed().subscribe((confirmDelete) => {
      if (confirmDelete) {
        this.isLoaded = false;
        this.userService.remove(this.data.user.id).subscribe(
          (res) => {
            this.snackBar.open(`Usuário removido`, 'ok', {
              duration: 2000,
              horizontalPosition: 'center',
              verticalPosition: 'bottom',
            });
          },
          (err) => {
            this.snackBar.open(`Erro ao remover o usuário`, 'ok', {
              duration: 2000,
              horizontalPosition: 'center',
              verticalPosition: 'bottom',
            });
            console.warn('post error', err);
          },
          () => {
            this.isLoaded = true;
            dialogRefConfirm.close();
            this.dialogRef.close();
          }
        );
      } else {
        dialogRefConfirm.close();
      }
    });
  }

  get name() {
    return this.userForm.get('name');
  }

  get email() {
    return this.userForm.get('email');
  }

  get role() {
    return this.userForm.get('role');
  }

  get password() {
    return this.userForm.get('password');
  }

  get company() {
    return this.userForm.get('company');
  }
}
