import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from "../_services";
@Component({
  selector: 'app-registers',
  templateUrl: './registers.component.html',
  styleUrls: ['./registers.component.scss'],
})
export class RegistersComponent implements OnInit {
  userRole: string;
  constructor(
    private authService: AuthenticationService,
  ) {}

  ngOnInit(): void {
    this.userRole = this.authService.currentUser.role;
  }
}
