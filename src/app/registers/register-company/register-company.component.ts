import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { CompanyModel } from '../../_models';
import { CompaniesService } from '../../_services';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NewCompanyComponent } from './new-company/new-company.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-register-company',
  templateUrl: './register-company.component.html',
  styleUrls: ['./register-company.component.scss'],
})
export class RegisterCompanyComponent implements AfterViewInit {
  displayedColumns: string[] = ['name', 'initials', 'desc'];
  companies: CompanyModel[];
  dataSource: MatTableDataSource<CompanyModel>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private companiesService: CompaniesService, public dialog: MatDialog) {}

  ngAfterViewInit(): void {
    this.reloadCompanies();
  }

  reloadCompanies() {
    this.companiesService.getAllCompanies().subscribe(
      (response) => {
        this.companies = response['rows'];
        this.dataSource = new MatTableDataSource(this.companies);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  editCompany(company: CompanyModel) {
    const dialogRef = this.dialog.open(NewCompanyComponent, {
      data: {company},
      width: '25em'
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.reloadCompanies();
    });
  }

  newCompany() {
    const dialogRef = this.dialog.open(NewCompanyComponent, {
      data: {},
      width: '25em'
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.reloadCompanies();
    });
  }
}
