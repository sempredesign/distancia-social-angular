import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CompaniesService } from '../../../_services';

@Component({
  selector: 'app-new-company',
  templateUrl: './new-company.component.html',
  styleUrls: ['./new-company.component.scss'],
})
export class NewCompanyComponent implements OnInit {
  companyForm: any;
  showErrors = false;
  mode: string = 'create'; // or 'edit'
  title: string = 'Cadastrar nova instituição'
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private companiesService: CompaniesService
  ) {
  }

  ngOnInit(): void {
    if (this.data.company) {
      this.mode = 'edit';
      this.title = 'Editar instituição';
    } else {
      this.mode = 'create';
      this.title = 'Cadastrar nova instituição';
    }
    
    this.companyForm = this.formBuilder.group({
      name: new FormControl(
        this.mode === 'edit' ? this.data.company.name : '',
        [Validators.required, Validators.maxLength(32)]
      ),
      initials: new FormControl(
        this.mode === 'edit' ? this.data.company.initials : '',
        [Validators.required, Validators.maxLength(8)]
      ),
      desc: new FormControl(
        this.mode === 'edit' ? this.data.company.desc : '',
        [Validators.required, Validators.maxLength(64)]
      ),
    });
  }

  onSubmit() {
    this.showErrors = true;
    this.companiesService
      .save(
        this.companyForm.value,
        this.mode === 'edit' ? this.data.company.id : null
      )
      .subscribe(
        (res) => {
          this.snackBar.open(
            `Instituição ${this.mode === 'edit' ? 'Editada' : 'Cadastrada'}`,
            'ok',
            {
              duration: 2000,
              horizontalPosition: 'center',
              verticalPosition: 'bottom',
            }
          );
        },
        (err) => {
          this.snackBar.open(
            `Erro ao ${
              this.mode === 'edit' ? 'editar' : 'cadastrar'
            } a instituição`,
            'ok',
            {
              duration: 2000,
              horizontalPosition: 'center',
              verticalPosition: 'bottom',
            }
          );
          console.warn('post error', err);
        }
      );
  }

  getErrorMessage(e) {
    if (!e) return;
    if (e.required)
      return 'Campo necessário';
    if (e.mminlength)
      return `Use pelo menos ${this.name.errors.mminlength.requiredLength} carateres`;
    if (e.maxlength)
      return `Use até ${this.name.errors.maxlength.requiredLength} carateres`;
    return 'Entrada inválida';
  }

  get name() {
    return this.companyForm.get('name');
  }

  get initials() {
    return this.companyForm.get('initials');
  }

  get desc() {
    return this.companyForm.get('desc');
  }
}
