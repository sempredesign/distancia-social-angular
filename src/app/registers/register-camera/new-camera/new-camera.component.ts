import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CameraService, GeolocationService } from '../../../_services';
import { DialogContentMap } from '../../../_shared/map-selector.component';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import {
  ConfirmDialogComponent,
  ConfirmDialogModel,
} from '../../../_shared/confirm-dialog.component';

export function urlValidator(c: FormControl) {
  if (!c.value) {
    return null;
  }
  const control: HTMLInputElement = document.createElement('input');
  control.type = 'url';
  control.value = c.value;
  const isValid: boolean = control.checkValidity();
  return isValid ? null : { invalidUrl: { value: c.value } };
}

export function latlngValidator(c: FormControl) {
  if (!c.value) {
    return null;
  }
  return c.value >= -90 && c.value <= 90
    ? null
    : { invalidGeo: { value: c.value } };
}
@Component({
  selector: 'app-new-camera',
  templateUrl: './new-camera.component.html',
  styleUrls: ['./new-camera.component.scss'],
})
export class NewCameraComponent implements OnInit {
  cameraForm: any;
  showErrors = false;
  mode: string = 'create'; // or 'edit'
  title = 'Cadastrar Nova Câmera';
  isLoaded: boolean = false;
  states = {
    Acre: 'AC',
    Alagoas: 'AL',
    Amapa: 'AP',
    Amazonas: 'AM',
    Bahia: 'BA',
    Ceara: 'CE',
    'Distrito Federal': 'DF',
    'Espirito Santo': 'ES',
    Goías: 'GO',
    Maranhao: 'MA',
    'Mato Grosso': 'MT',
    'Mato Grosso do Sul': 'MS',
    'Minas Gerais': 'MG',
    Para: 'PA',
    Paraiba: 'PB',
    Parana: 'PR',
    Pernambuco: 'PE',
    Piaui: 'PI',
    'Rio de Janeiro': 'RJ',
    'Rio Grande do Norte': 'RN',
    'Rio Grande do Sul': 'RS',
    Rondonia: 'RO',
    Roraima: 'RR',
    'Santa Catarina': 'SC',
    'Sao Paulo': 'SP',
    Sergipe: 'SE',
  };

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private camerasService: CameraService,
    private geolocationService: GeolocationService,
    private dialogRef: MatDialogRef<NewCameraComponent>
  ) {}

  ngOnInit(): void {
    if (this.data.camera) {
      this.mode = 'edit';
      this.title = 'Editar Câmera';
    } else {
      this.mode = 'create';
      this.title = 'Cadastrar Nova Câmera';
    }

    this.cameraForm = this.formBuilder.group({
      name: new FormControl(this.mode === 'edit' ? this.data.camera.name : '', [
        Validators.required,
        Validators.maxLength(8),
      ]),
      urlPrimary: new FormControl(
        this.mode === 'edit' ? this.data.camera.urlPrimary : '',
        [urlValidator]
      ),
      urlSecondary: new FormControl(
        this.mode === 'edit' ? this.data.camera.urlSecondary : '',
        [urlValidator]
      ),
      desc: new FormControl(this.mode === 'edit' ? this.data.camera.desc : '', [
        Validators.maxLength(64),
      ]),
      city: new FormControl(this.mode === 'edit' ? this.data.camera.city : '', [
        Validators.required,
        Validators.maxLength(32),
      ]),
      state: new FormControl(
        this.mode === 'edit' ? this.data.camera.state : '',
        [Validators.required]
      ),
      lat: new FormControl(this.mode === 'edit' ? this.data.camera.lat : '', [
        Validators.required,
        latlngValidator,
      ]),
      lng: new FormControl(this.mode === 'edit' ? this.data.camera.lng : '', [
        Validators.required,
        latlngValidator,
      ]),
    });
    this.isLoaded = true;
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogContentMap);
    dialogRef.afterClosed().subscribe((result) => {
      this.cameraForm.get('lat').setValue(result.lat);
      this.cameraForm.get('lng').setValue(result.lng);
    });
  }

  getUserLocation() {
    this.isLoaded = false;
    this.geolocationService.getCurrentLocation().subscribe(
      (res) => {
        this.cameraForm.get('city').setValue(res['city']);
        this.cameraForm.get('lat').setValue(res['latitude']);
        this.cameraForm.get('lng').setValue(res['longitude']);
        this.cameraForm.get('state').setValue(this.states[res['state']]);
      },
      (err) => {
        // use browser to get lat lng:
        navigator.geolocation.getCurrentPosition(
          (position) => {
            alert('Local acessado');
            this.cameraForm.get('lat').setValue(position.coords['latitude']);
            this.cameraForm.get('lng').setValue(position.coords['longitude']);
            this.isLoaded = true;
          },
          () => {
            alert('Acesso à localização não permitido');
          },
          // { timeout: 10000 }
        );
      },
      () => {
        this.isLoaded = true;
      }
    );
  }

  onSubmit() {
    this.isLoaded = false;
    this.showErrors = true;
    this.camerasService
      .save(
        this.cameraForm.value,
        this.mode === 'edit' ? this.data.camera.id : null
      )
      .subscribe(
        (res) => {
          this.snackBar.open(
            `Câmera ${this.mode === 'edit' ? 'Editada' : 'Cadastrada'}`,
            'ok',
            {
              duration: 2000,
              horizontalPosition: 'center',
              verticalPosition: 'bottom',
            }
          );
        },
        (err) => {
          let message = `Erro ao ${
            this.mode === 'edit' ? 'editar' : 'cadastrar'
          } câmera`;
          if (err.error)
            if (err.error.errorMsg)
              if (
                err.error.errorMsg.includes('duplicado') &&
                err.error.errorMsg.includes('url')
              )
                message += ' . Uma Câmera com essa URL já existe no sistema.';
              else message += " . " + err.error.errorMsg;

          this.snackBar.open(message, 'ok', {
            duration: 6000,
            horizontalPosition: 'center',
            verticalPosition: 'bottom',
          });
          this.dialogRef.close();
        },
        () => {
          this.isLoaded = true;
          this.dialogRef.close();
        }
      );
  }

  getErrorMessage(e) {
    if (!e) return;
    if (e.required) return 'Campo necessário';
    if (e.mminlength)
      return `Use pelo menos ${e.minlength.requiredLength} carateres`;
    if (e.maxlength) return `Use até ${e.maxlength.requiredLength} carateres`;
    if (e.invalidUrl) return `Url inválido.`;
    if (e.invalidGeo) return `Coordenada inválida.`;
    return 'Entrada inválida';
  }

  deleteCamera() {
    const message = `Deseja remover esta câmera?`;
    const dialogData = new ConfirmDialogModel('Confirmar Ação', message);
    const dialogRefConfirm = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '400px',
      data: dialogData,
    });
    dialogRefConfirm.afterClosed().subscribe((confirmDelete) => {
      if (confirmDelete) {
        this.isLoaded = false;
        this.camerasService.remove(this.data.camera.id).subscribe(
          (res) => {
            this.snackBar.open(`Câmera removida`, 'ok', {
              duration: 2000,
              horizontalPosition: 'center',
              verticalPosition: 'bottom',
            });
          },
          (err) => {
            this.snackBar.open(`Erro ao remover a câmera`, 'ok', {
              duration: 2000,
              horizontalPosition: 'center',
              verticalPosition: 'bottom',
            });
            console.warn('post error', err);
          },
          () => {
            this.isLoaded = true;
            dialogRefConfirm.close();
            this.dialogRef.close();
          }
        );
      } else {
        dialogRefConfirm.close();
      }
    });
  }

  get name() {
    return this.cameraForm.get('name');
  }

  get urlPrimary() {
    return this.cameraForm.get('urlPrimary');
  }

  get urlSecondary() {
    return this.cameraForm.get('urlSecondary');
  }

  get desc() {
    return this.cameraForm.get('desc');
  }

  get city() {
    return this.cameraForm.get('city');
  }

  get state() {
    return this.cameraForm.get('state');
  }

  get lat() {
    return this.cameraForm.get('lat');
  }

  get lng() {
    return this.cameraForm.get('lng');
  }
}
