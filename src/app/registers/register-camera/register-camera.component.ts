import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { CameraModel } from '../../_models';
import { CameraService } from '../../_services';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NewCameraComponent } from './new-camera/new-camera.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-register-camera',
  templateUrl: './register-camera.component.html',
  styleUrls: ['./register-camera.component.scss'],
})
export class RegisterCameraComponent implements AfterViewInit {
  displayedColumns: string[] = [
    'name',
    'city',
    'lat',
    'lng',
    'urlPrimary',
    'urlSecondary',
    'calibrated',
    'active',
    'online',
  ];
  columnLabels = {
    'name': 'Nome',
    'city': 'Cidade',
    'lat': 'Lat',
    'lng': 'Lng',
    'urlPrimary': 'Url Detecção',
    'urlSecondary': 'Url Stream',
    'calibrated': 'Calibrada',
    'active': 'Ativa',
    'online': 'Online',
  };
  camerasCompany: CameraModel[];
  dataSource: MatTableDataSource<CameraModel>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private cameraService: CameraService, public dialog: MatDialog) {}

  ngAfterViewInit(): void {
    this.reloadCameras();  
  }

  reloadCameras() {
    this.cameraService.getAllCameras().subscribe(
      (response) => {
        this.camerasCompany = response['rows'];
        this.dataSource = new MatTableDataSource(this.camerasCompany);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  editCamera(camera: CameraModel) {
    const dialogRef = this.dialog.open(NewCameraComponent, {
      data: {camera},
      width: '30em',
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.reloadCameras();
    });
  }

  newCamera() {
    const dialogRef = this.dialog.open(NewCameraComponent, {
      data: {},
      width: '30em'
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.reloadCameras();
    });
  }
}
