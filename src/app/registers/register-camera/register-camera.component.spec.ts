import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterCameraComponent } from './register-camera.component';

describe('RegisterCameraComponent', () => {
  let component: RegisterCameraComponent;
  let fixture: ComponentFixture<RegisterCameraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterCameraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterCameraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
