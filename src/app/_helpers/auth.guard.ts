import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';

import { AuthenticationService } from '../_services';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // const user = this.authenticationService.userValue;
    // if (user) {
    //   // logged in so return true
    //   return true;

    if (this.authenticationService.isLoggedIn()) {
      let role = this.authenticationService.currentUser.role;
      let allowed: boolean;
      switch (state.url) {
        case '/':
          allowed = true;
          break;
        case '/cameras':
          allowed = true;
          break;
        case '/registers':
          allowed = role === 'admin' || role === 'manager';
          break;
        case '/login':
          allowed = true;
          break;
        default:
          allowed = false;
          break;
      }
      return allowed;
    } else {
      // not logged in so redirect to login page with the return url
      this.router.navigate(['/login'], {
        queryParams: { returnUrl: state.url },
      });
      return false;
    }
  }
}
