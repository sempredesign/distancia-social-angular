import { Component, AfterViewInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as L from 'leaflet';

@Component({
  selector: 'map-selector',
  templateUrl: 'map-selector.component.html',
  styleUrls: ['./map-selector.component.scss'],
})
export class DialogContentMap implements AfterViewInit {
  map: L.Map;
  selectedCoords: L.LatLng;
  layerGroup: L.LayerGroup;
  markerIcon = {
    icon: L.icon({
      iconSize: [25, 41],
      iconAnchor: [10, 41],
      popupAnchor: [2, -40],
      iconUrl: "https://unpkg.com/leaflet@1.5.1/dist/images/marker-icon.png"
    })
  };
  
  ngAfterViewInit() {
    this.map = L.map("map", {
      preferCanvas: true,
      zoomControl: false,
      maxZoom: 19,
      keyboard: false,
      layers: [],
    });
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(this.map);
    this.map.fitBounds([
      [-20.2894591, -40.2926235],
      [-20.2897025, -40.2920615],
    ]);
    this.layerGroup = L.layerGroup().addTo(this.map);
    this.map.setZoom(10);
    this.map.on("click", (e: L.LeafletMouseEvent) => {
      this.layerGroup.clearLayers();
      this.selectedCoords = e.latlng;
      L.marker([e.latlng.lat, e.latlng.lng], this.markerIcon).addTo(
        this.layerGroup
      );
    });
  }
}
