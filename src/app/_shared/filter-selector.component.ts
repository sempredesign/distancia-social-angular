import { Component, OnInit, Inject } from '@angular/core';
import {
  MatDialog,
  MatDialogModule,
  MatDialogContent,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSelectModule, MatSelect } from '@angular/material/select'; 

@Component({
  selector: 'filter-selector',
  templateUrl: 'filter-selector.component.html',

})
export class DialogContentFilter implements OnInit {
  selectedSeverity: Number = 0;
  selectedSituation: Number = 0;

  constructor(@Inject(MAT_DIALOG_DATA) public data) {
    this.selectedSeverity = data.selectedSeverity;
    this.selectedSituation = data.selectedSituation;
  }

  selectOptionSeverity(value) {
    this.selectedSeverity = value;
  }

  selectOptionSituation(value) {
    this.selectedSituation = value;
  }

  ngOnInit() {}
}
