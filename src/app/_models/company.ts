export interface CompanyModel {
  id?: string;
  name?: string;
  desc?: string;
  initials?: string;
}
