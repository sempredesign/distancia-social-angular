export interface CameraModel {
  id?: string;
  user?: string;
  records?: any[];
  name?: string;
  desc?: string;
  lat?: number;
  lng?: number;
  city?: string;
  state?: string;
  primaryWidth?: number;
  primaryHeight?: number;
  secondaryWidth?: number;
  secondaryHeight?: number;
  urlPrimary?: string;
  urlSecondary?: string;
  calibrated?: boolean;
  active?: boolean;
  online?: boolean;
  showOnMap?: boolean;
  status?: string;
  calibrationPoints?: number[];
  createdAt ?: Date;
  updatedAt?: Date;
  marker?: any;
  selected?: boolean;
  severityMean?: number;
  severityMeanStr?: string;
}
