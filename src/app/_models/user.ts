import { CompanyModel } from './company';
export interface UserModel {
  id?: string;
  password?: string;
  email?: string;
  company?: CompanyModel;
  role?: string;
  theme?: string;
  token?: string;
  name?: string;
}
