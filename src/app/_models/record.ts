export interface RecordModel {
  id?: string;
  user?: string;
  camera?: string;
  name?: string;
  severityLevel?: number;
  peopleDetected?: number;
  facesDetected?: number;
  crowdNumber?: number;
  noMaskNumber?: number;
  imgPath?: string;
  failed?: boolean;
  createdAt?: Date;
}
