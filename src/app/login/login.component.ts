import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthenticationService } from '../_services';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  showPassword = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private snackBar: MatSnackBar
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.isLoggedIn()) {
      this.router.navigate(['']);
    }
  }

  ngOnInit() {
    if (this.authenticationService.isLoggedIn()) {
      this.router.navigate(['']);
    }
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.returnUrl = '/';
  }

  onSubmit(credentials) {
    this.submitted = true;

    // stop here if form is invalid
    if (credentials.invalid) {
      return;
    }

    this.authenticationService
      .login(credentials.username, credentials.password)
      .pipe(first())
      .subscribe({
        next: () => {
          window.location.reload();
          this.router.navigate([this.returnUrl]);
        },
        error: (error) => {
          this.error = error;
          this.snackBar.open("Login inválido", "ok", {
            duration: 2000,
            horizontalPosition: "center",
            verticalPosition: "top"
          });
        },
      });
  }

  togglePassword() {
    this.showPassword = !this.showPassword;
  }
}
