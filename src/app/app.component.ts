import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService, CompaniesService } from './_services';
import { UserModel, CompanyModel } from './_models';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { RouterEvent } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  user: UserModel;
  userRole: string;
  isLoggedIn = false;
  companies: CompanyModel[];

  roleLabel = {
    'manager': 'Gerente',
    'user': 'Operador',
    'admin': 'Admin'
  }
  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private companiesService: CompaniesService
  ) {
    this.matIconRegistry.addSvgIcon(
      'camera',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        '../../assets/images/ipcamera.svg'
      )
    );
    // this.authService.user.subscribe((x) => (this.user = x));
    const token = localStorage.getItem('token');
    if (token) {
      this.isLoggedIn = authService.isLoggedIn();
      this.user = authService.currentUser;
      this.userRole = this.user.role;
      this.UserMenu.name = this.user.name;
      this.UserMenu.role = this.user.role;
      // authService.userReady.subscribe((isLoggedIn: boolean) => {
      //   this.isLoggedIn = isLoggedIn;
      //   if (isLoggedIn) {
      //     this.user = authService.currentUser;
      //   } else {
      //     this.user = null;
      //   }
      // });
      this.companiesService.getAllCompanies().subscribe(
        (response) => {
          this.companies = response['rows'];
          this.UserMenu.companies = this.companies;
        }
        );
      }
      router.events.subscribe((e: RouterEvent) => {
        this.isLoggedIn = authService.isLoggedIn();
        if (e.url) {
          this.NavMenu.selecteditem = e.url;
        }
      });
  }
  
  changeCompany(company) {
    this.authService.changeCompany(company.id).subscribe(res => {
      localStorage.removeItem("filtroLista");
    });
  }
  
  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
  
  ngOnInit() {
    
  }
  
  // html custom components attributes
  NavMenu = {
    selecteditem: '',//this.router.currentUrlTree.toString(),
    visible: false,
    handleGoTo: (event) => {
      this.router.navigateByUrl(event.detail);
    }
  }
  
  UserMenu = {
    name: '',
    role: '',
    companies: [],
    handleLogout: () => {
      this.logout();
    },
    handleChangeCompany: (event) => {
      console.log(event.detail.name);
      this.changeCompany(event.detail);
    }
  }
  
  HelpMenu = {
    handleGoTo: (event) => {
      this.router.navigateByUrl(event.detail);
    }
  }


}